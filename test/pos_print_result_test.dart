import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  test('success', () {
    expect(PosPrintResult.success.msg, 'Success');
  });

  test('timeout', () {
    expect(PosPrintResult.timeout.msg, 'Error. Printer connection timeout');
  });

  test('printerNotSelected', () {
    expect(
        PosPrintResult.printerNotSelected.msg, 'Error. Printer not selected');
  });

  test('ticketEmpty', () {
    expect(PosPrintResult.ticketEmpty.msg, 'Error. Ticket is empty');
  });

  test('printInProgress', () {
    expect(
        PosPrintResult.printInProgress.msg, 'Error. Another print in progress');
  });

  test('scanInProgress', () {
    expect(PosPrintResult.scanInProgress.msg,
        'Error. Printer scanning in progress');
  });
}
